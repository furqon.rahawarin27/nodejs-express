const express = require('express')
const router = express.Router()

// CRUD => Basic function
// Create
// Read
// Update
// Delete

let dataProduct = [
    {
        id : 1,
        namaProduct : "Laptop ROG",
        namaBrand : "Asus",
        jumlahKetersediaan : 100
    },
    {
        id : 2,
        namaProduct : "Laptop ROG",
        namaBrand : "Apple",
        jumlahKetersediaan : 200
    },
    {
        id : 3,
        namaProduct : "Laptop ROG",
        namaBrand : "Asus",
        jumlahKetersediaan : 300
    },
    {
        id : 4,
        namaProduct : "Laptop ROG",
        namaBrand : "Lenovo",
        jumlahKetersediaan : 400
    }
]

// API yang dimana bisa deliver sebuah requestt untuk membuat / create sebuah produk
router.post('/api/v1/product', (req, res) => {
    const {namaProduct, namaBrand, jumlahKetersediaan} = req.body

    if(namaProduct === undefined || namaBrand === undefined || jumlahKetersediaan === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }
    const Id = Math.random().toString(36).substring(2);

    dataProduct.push({
        id : Id,
        namaProduct : namaProduct,
        namaBrand : namaBrand,
        jumlahKetersediaan : jumlahKetersediaan
    })

    res.status(200).json("Succeed!")
});

// API yang dimana bisa deliver sebuah request untuk melihat semua data produk
router.get('/api/v1/product', (req, res) => {
    res.json(dataProduct)
});

// API yang dimana bisa deliver sebuah request untuk melihat data produk berdasarkan ID
router.get('/api/v1/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    const filteredProduct = dataProduct.find(x => x.id == idProduct)

    // Validasi pencarian data
    if(filteredProduct === undefined){
        res.status(404).json("Data product is not found!")
        return
    }
    res.status(200).json(filteredProduct)
})

router.get('/api/v1/product/filter', (req, res) => {
    const namaBrand = req.params.namaBrand
    const filteredProduct = dataProduct.filter(function (filter){
        return filter.jumlahKetersediaan >= 300;
    });

    console.log(filteredProduct)
    // Validasi pencarian data
    if(filteredProduct === undefined){
        res.status(404).json("Data product is not found!")
        return
    }
    res.status(200).json(filteredProduct)
})

// API yang dimana bisa deliver sebuah request untuk mengubah data produk berdasarkan ID
router.put('/api/v1/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    const {namaProduct, namaBrand, jumlahKetersediaan} = req.body

    if(namaProduct === undefined || namaBrand === undefined || jumlahKetersediaan === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }

    let isFound = false
    for(var x = 0; x<dataProduct.length; x++){
        if(dataProduct[x].id == idProduct){
            dataProduct[x].namaProduct = namaProduct
            dataProduct[x].namaBrand = namaBrand
            dataProduct[x].jumlahKetersediaan = jumlahKetersediaan

            isFound = true
            break
        }
    }

    if(isFound == false){
        res.status(404).json("Data product is not found!")
        return
    }else{
        res.status(200).json("Updated!")
        return
    }

})

// API yang dimana bisa deliver sebuah request untuk mendelete semua data / beberapa data produk
router.delete('/api/v1/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct

    const searchProduct = dataProduct.find(x => x.id == idProduct)

    if (searchProduct === undefined){
        res.status(404).json("Data product is not found!")
        return
    }

    const index = dataProduct.indexOf(searchProduct)
    dataProduct.splice(index,1)

    res.status(200).json("Deleted")
});

module.exports = router