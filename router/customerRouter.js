const express = require('express')
const router = express.Router()

let dataCustomer = [
    {
        id :1, // => Auto increment
        nama : "Dezan",
        alamat : "Bekasi"
    },
    {
        id :2,
        nama : "Luthfi",
        alamat : "Jakarta"
    },
    {
        id :3,
        nama : "Dennis",
        alamat : "Bogor"
    }
]

// API yang dimana bisa deliver sebuah requestt untuk membuat / create sebuah produk
router.post('/api/customer', (req, res) => {
    const {nama, alamat} = req.body

    if(nama === undefined || alamat === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }
    const nextId = dataCustomer.length + 1

    dataCustomer.push({
        id : nextId,
        nama : nama,
        alamat : alamat
    })

    res.status(200).json("Succeed!")
})

// API yang dimana bisa deliver sebuah request untuk melihat semua data produk
router.get('/api/customer', (req, res) => {
    res.status(200).json(dataCustomer)
})

router.get('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer
    const filteredCustomer = dataCustomer.find(x => x.id == idCustomer)

    // Validasi pencarian data
    if(filteredCustomer === undefined){
        res.status(404).json("Data customer is not found!")
        return
    }
    res.status(200).json(filteredCustomer)
})

// API yang dimana bisa deliver sebuah request untuk mengubah semua data produk
router.put('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer
    const {nama, alamat} = req.body

    if(nama === undefined || alamat === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }

    let isFound = false
    for(var x = 0; x<dataCustomer.length; x++){
        if(dataCustomer[x].id == idCustomer){
            dataCustomer[x].nama = nama
            dataCustomer[x].alamat = alamat

            isFound = true
            break
        }
    }

    if(isFound == false){
        res.status(404).json("Data customer is not found!")
        return
    }else{
        res.status(200).json("Updated!")
        return
    }

})

// API yang dimana bisa deliver sebuah request untuk mendelete semua data / beberapa data produk
router.delete('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer

    const searchCustomer = dataCustomer.find(x => x.id == idCustomer)

    if (searchCustomer === undefined){
        res.status(404).json("Data customer is not found!")
        return
    }
    
    // return index dari variable searchCustomer
    const index = dataCustomer.indexOf(searchCustomer)

    // Argument (start) = Ngehapus element array dari index berapa?
    // Argument (end) = Mau menghapus sebanyak berapa data?
    dataCustomer.splice(index,1)

    res.status(200).json("Deleted")
})

module.exports = router